#!/bin/bash


export repo="$1"
export regex="$2"

if [ "$regex" == "" ]; then
  # Default semver regex
  regex='^v?([0-9]+)\.([0-9]+)\.([0-9]+)$'
fi

# Get tags from upstream
upstream_json_resp="$(skopeo list-tags docker://${repo})"
if [ $? -ne 0 ]; then
  echo "Failed to list upstream tags"
  exit 1
fi
image_name=$(basename "${repo}")
tags_to_sync=$(echo "${upstream_json_resp}" | jq -r --arg regex "$regex" '.Tags[] | select(test($regex))')

# Get tags from CERN (GITLAB)
cern_json_resp="$(skopeo list-tags docker://${CI_REGISTRY_IMAGE}/${image_name})"
if [ $? -ne 0 ]; then
  if ! $(skopeo list-tags "docker://${CI_REGISTRY_IMAGE}/${image_name}" 2>&1 | grep "404 (Not Found)" -q); then
    echo "Failed to list gitlab tags"
    exit 1
  fi

  # This seems to be a new image, not found in gitlab-registry.cern.ch.
  # Continue gracefully.
  cern_json_resp=""
fi
cern_repo_tags="$(echo "${cern_json_resp}" | jq .Tags[] -r | xargs)"

# Start update process (GITLAB)
echo "start to sync ${repo} to gitlab"
for tag in $(echo "${tags_to_sync[@]}" ); do
  echo -n "syncing ${repo}:${tag}"
  i=0

  # If the container tag does not exist in gitlab
  if [[ ! " ${cern_repo_tags[@]} " =~ " ${tag} " ]]; then
    # Update
    echo ""
    until skopeo copy --format v2s2 --dest-creds "gitlab-ci-token:${CI_BUILD_TOKEN}" "docker://${repo}:${tag}" "docker://${CI_REGISTRY_IMAGE}/${image_name}:${tag}"
    do
      ((i++))
      if [ $i -gt 5 ] ; then
        break
      fi
      sleep 2
    done
  else
    # Nothing to do
    echo " - exists"
  fi
done

# Get tags from CERN (HARBOR)
harbor_json_resp="$(skopeo list-tags docker://registry.cern.ch/magnum/${image_name})"
if [ $? -ne 0 ]; then
  if ! $(skopeo list-tags "docker://registry.cern.ch/magnum/${image_name}" 2>&1 | grep "404 (Not Found)" -q); then
    echo "Failed to list harbor tags"
    exit 1
  fi

  # This seems to be a new image, not found in registry.cern.ch.
  # Continue gracefully.
  harbor_json_resp=""
fi
harbor_repo_tags="$(echo "${harbor_json_resp}" | jq .Tags[] -r | xargs)"

# Start update process (HARBOR)
echo "start to sync ${repo} to harbor"
for tag in $(echo "${tags_to_sync[@]}" ); do
  echo -n "syncing ${repo}:${tag}"
  i=0

  # If the container tag does not exist in harbor
  if [[ ! " ${harbor_repo_tags[@]} " =~ " ${tag} " ]]; then
    # Update
    echo ""
    until skopeo copy --dest-creds "${HARBOR_ROBOT_NAME}:${HARBOR_ROBOT_TOKEN}" "docker://${CI_REGISTRY_IMAGE}/${image_name}:${tag}" "docker://registry.cern.ch/magnum/${image_name}:${tag}"
    do
      ((i++))
      if [ $i -gt 5 ] ; then
        break
      fi
      sleep 2
    done
  else
    # Nothing to do
    echo " - exists"
  fi
done
echo "end syncing ${repo}"
