#!/bin/sh

. /etc/kubernetes/controller-manager
. /etc/kubernetes/config

ARGS="$@ $KUBE_LOGTOSTDERR $KUBE_LOG_LEVEL $KUBE_MASTER $KUBE_CONTROLLER_MANAGER_ARGS"

ARGS=$(echo $ARGS | sed s/--cloud-provider=openstack//)
ARGS=$(echo $ARGS | sed s#--cloud-config=/etc/kubernetes/kube_openstack_config##)
ARGS="${ARGS} --secure-port=0"

exec /usr/local/bin/kube-controller-manager $ARGS
